package debugging.assignment;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // System.out.println( "Hello World!" );
        
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);

        // change it so that it's a different company

        employees[0].setEmployeeId(12);

        Company c2 = new Company(employees);
        // assertNotEquals(c1, c2);
        System.out.println(c1.equals(c2));
    }
}
